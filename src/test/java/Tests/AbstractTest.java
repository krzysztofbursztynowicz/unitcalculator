package Tests;

import actions.Assertions;
import actions.CalculatorActions;
import config.ConfigActions;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.*;

public abstract class AbstractTest {

    WebDriver driver;
    JavascriptExecutor executor;
    WebDriverWait wait;
    public CalculatorActions calculatorActions;
    public Assertions assertions;

    @Parameters({"browser", "shallRunHeadless"})
    @BeforeClass
    protected void beforeTest(String browser, String shallRunHeadless) {
        if (browser.equals("chrome")) { this.driver= ConfigActions.chromeInit(shallRunHeadless); }
        else if (browser.equals("firefox")){ this.driver= ConfigActions.firefoxInit(); }
        this.executor = (JavascriptExecutor) driver;
        this.wait = new WebDriverWait(driver, 2);
        this.calculatorActions= new CalculatorActions(driver, wait);
        this.assertions = new Assertions(calculatorActions);
    }

    @Parameters({"baseURL"})
    @BeforeMethod
    public void beforeClass(String baseURL) {
        driver.get(baseURL);
    }

    @AfterClass
    protected void afterTest(){
        driver.quit();
    }
}
