package Tests;

import org.testng.annotations.Test;

public class AreaTabTests extends AbstractTest {

    @Test(dataProvider="hectaresToSquareYards", dataProviderClass = dataProviders.ComputationDataProvider.class)
    public void should_compute_hectares_to_squareyards_correctly(
            String baseUnit,
            String finalUnit,
            double valueToConvert,
            double ratio
    ) {
        //given
        calculatorActions.selectTab("Area");
        //when
        calculatorActions.calculationSteps(baseUnit, finalUnit, valueToConvert);
        //then
        assertions.shouldMultiplyBaseUnitAndRatio(valueToConvert, ratio);
    }
}
