package Tests;
import dataProviders.ComputationDataProvider;
import org.testng.annotations.*;

public class LengthTabTests extends AbstractTest {

    @Test(dataProvider="metersToMillimeters", dataProviderClass = ComputationDataProvider.class)
    public void should_compute_meter_to_millimeter_correctly(
            String baseUnit,
            String finalUnit,
            double valueToConvert,
            double ratio
    ) {
        //given
        calculatorActions.selectTab("Length");
        //when
        calculatorActions.calculationSteps(baseUnit, finalUnit, valueToConvert);
        //then
        assertions.shouldMultiplyBaseUnitAndRatio(valueToConvert, ratio);
    }


}
