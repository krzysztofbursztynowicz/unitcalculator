package Tests;

import org.testng.annotations.Test;

public class TemperatureTabTests extends AbstractTest {

    @Test(dataProvider="fahrenheitToKelvin", dataProviderClass = dataProviders.ComputationDataProvider.class)
    public void should_compute_Fahrenheit_to_Kelvin_correctly(
            String baseUnit,
            String finalUnit,
            double valueToConvert
    ) {
        //given
        calculatorActions.selectTab("Temperature");
        //when
        calculatorActions.calculationSteps(baseUnit, finalUnit, valueToConvert);
        //then
        assertions.shouldComputeFahrToKelvin(valueToConvert);
    }
}
