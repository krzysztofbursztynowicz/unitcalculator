package Tests;

import org.testng.annotations.Test;

public class TimeTabTests extends AbstractTest {

    @Test(dataProvider="minutesToMilliseconds", dataProviderClass = dataProviders.ComputationDataProvider.class)
    public void should_compute_minutes_to_milliseconds_correctly(
            String baseUnit,
            String finalUnit,
            double valueToConvert,
            double ratio
    ) {
        //given
        calculatorActions.selectTab("Time");
        //when
        calculatorActions.calculationSteps(baseUnit, finalUnit, valueToConvert);
        //then
        assertions.shouldMultiplyBaseUnitAndRatio(valueToConvert, ratio);
    }
}
