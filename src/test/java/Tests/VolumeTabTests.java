package Tests;

import dataProviders.PageElementsDataProvider;
import org.testng.annotations.Test;

import java.util.List;

public class VolumeTabTests extends AbstractTest {

    @Test(dataProvider="volumeBaseUnits", dataProviderClass = PageElementsDataProvider.class)
    public void should_display_volume_base_units_correctly(List<String> elements) {
        //given
        calculatorActions.selectTab("Volume");
        //then
        assertions.shouldMatchElements(elements);
    }
}
