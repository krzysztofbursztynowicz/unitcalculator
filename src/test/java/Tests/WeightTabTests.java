package Tests;

import org.testng.annotations.Test;

public class WeightTabTests extends AbstractTest {

    @Test(dataProvider="poundsToCarats", dataProviderClass = dataProviders.ComputationDataProvider.class)
    public void should_compute_pounds_to_carats_correctly(
            String baseUnit,
            String finalUnit,
            double valueToConvert,
            double ratio
    ) {
        //given
        calculatorActions.selectTab("Weight");
        //when
        calculatorActions.calculationSteps(baseUnit, finalUnit, valueToConvert);
        //then
        assertions.shouldMultiplyBaseUnitAndRatio(valueToConvert, ratio);
    }
}
