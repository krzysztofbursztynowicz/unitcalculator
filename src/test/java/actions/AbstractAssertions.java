package actions;

import org.decimal4j.util.DoubleRounder;

import java.math.RoundingMode;

public class AbstractAssertions {

    public CalculatorActions calculatorActions;

    public double decimalPlacesTrimmer(double initValue) {
        return DoubleRounder.round(initValue, 8);
    }
}
