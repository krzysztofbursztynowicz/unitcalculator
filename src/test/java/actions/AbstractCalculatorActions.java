package actions;

import objects.CalculatorPageObject;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class AbstractCalculatorActions {

    protected CalculatorPageObject calculatorPageObject;
    protected WebDriverWait wait;
    JavascriptExecutor executor;

    public void jsClick(WebElement element) {
        executor.executeScript("arguments[0].click();", element);
    }

    //providing initial value
    public void provideInitValue(double value) { calculatorPageObject.fromInputField().sendKeys(String.valueOf(value)); }

    //reading calculated value
    //this method waits until a text appears in the textfield before it reads value
    public double readCalculatedValue() {
        wait.until((ExpectedCondition<Boolean>) d -> calculatorPageObject.toInputField().getText()!=null);
        return Double.parseDouble(calculatorPageObject.toInputField().getAttribute("value"));
    }

    //selecting initial unit
    public void selectInitUnit(String unitName) {
        new Select(calculatorPageObject.initialUnitContainer()).selectByVisibleText(unitName);
    }

    //selecting unit to be computed
    public void selectUnitToCompute(String unitName) {
        new Select(calculatorPageObject.desiredUnitContainer()).selectByVisibleText(unitName);

    }
}
