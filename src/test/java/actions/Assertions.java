package actions;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class Assertions extends AbstractAssertions {

    public Assertions(CalculatorActions calculatorActions) {
        this.calculatorActions=calculatorActions;
    }

    //generic assertion for computations utilising just multiplying
    public void shouldMultiplyBaseUnitAndRatio(double value, double ratio) {
        double result = value * ratio;
        assertThat(calculatorActions.readCalculatedValue()).isEqualTo(decimalPlacesTrimmer(result));
    }

    //assertion for fahrenheit->kelvin conversion (Temperature)
    public void shouldComputeFahrToKelvin(double value) {
        double result = (value+459.67)*5/9;
        assertThat(calculatorActions.readCalculatedValue()).isEqualTo(decimalPlacesTrimmer(result));
    }

    //assertion for check if base unit container contains all desired elements
    public void shouldMatchElements(List<String> elements) {
        assertThat(calculatorActions.volumeBaseUnitsToList().containsAll(elements)).isTrue();
    }
}
