package actions;

import objects.CalculatorPageObject;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class CalculatorActions extends AbstractCalculatorActions {

    public CalculatorActions(WebDriver driver, WebDriverWait wait) {
        this.calculatorPageObject = new CalculatorPageObject(driver);
        this.wait = wait;
        this.executor = (JavascriptExecutor) calculatorPageObject.driver;
    }

    //tab selection based on value provided
    //due to some issues regarding regular clicking, I've decided to use javascript executor
    public void selectTab(String tabName) {
        jsClick(wait.until(ExpectedConditions.elementToBeClickable(calculatorPageObject.tab(tabName))));
    }

    //calculation steps
    public void calculationSteps(String initUnit, String unitToBeCalculated, double value) {
        selectInitUnit(initUnit);
        selectUnitToCompute(unitToBeCalculated);
        provideInitValue(value);
    }

    public List<String> volumeBaseUnitsToList(){
        return calculatorPageObject.getVolumeBaseUnits();
    }
}