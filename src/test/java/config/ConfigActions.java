package config;

import io.github.bonigarcia.wdm.ChromeDriverManager;
import io.github.bonigarcia.wdm.FirefoxDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Parameters;

public class ConfigActions {

    //depends on parameters provided in the testng.xml, driver can be initialized for firefox, chrome or chrome headless mode
    public static WebDriver chromeInit(String shallRunHeadless) {
        ChromeDriverManager.getInstance().setup();

        if (shallRunHeadless.equalsIgnoreCase("yes")) {
            ChromeOptions options = new ChromeOptions();
            options.addArguments("--headless");
            options.addArguments("--disable-gpu");
            return new ChromeDriver(options);
        }

        else return new ChromeDriver();
    }

    public static WebDriver firefoxInit() {
        FirefoxDriverManager.getInstance().setup();
        return new FirefoxDriver();
    }
}
