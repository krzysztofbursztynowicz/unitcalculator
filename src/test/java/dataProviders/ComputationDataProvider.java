package dataProviders;

import org.testng.annotations.DataProvider;

public class ComputationDataProvider {

    //data provider for Length tab
    @DataProvider(name = "metersToMillimeters")
    public static Object[][] metersToMillimeters() {

        return new Object[][] {
                {"Meter", "Millimeter", 2000, 1000}
        };
    }

    //data provider for Area tab
    @DataProvider(name = "hectaresToSquareYards")
    public static Object[][] hectaresToSquareYards() {

        return new Object[][]{
                {"Hectare", "Square Yard", 2000, 11959.900463}
        };
    }

    //data provider for Temperature tab test (it requires just 3 parameters, no ratio needed)
    @DataProvider(name = "fahrenheitToKelvin")
    public static Object[][] fahrenheitToKelvin() {

        return new Object[][] {
                {"Fahrenheit", "Kelvin", 30}
        };
    }

    //data provider for Time tab
    @DataProvider(name = "minutesToMilliseconds")
    public static Object[][] minutesToMilliseconds() {

        return new Object[][]{
                {"Minute", "Millisecond", 555, 60000}
        };
    }

    //data provider for Weight tab
    @DataProvider(name = "poundsToCarats")
    public static Object[][] poundsToCarats() {

        return new Object[][]{
                {"Pound", "Carrat", 555, 2267.96}
        };
    }
}
