package dataProviders;

import edu.emory.mathcs.backport.java.util.Arrays;
import org.testng.annotations.DataProvider;

import java.util.ArrayList;
import java.util.List;

public class PageElementsDataProvider {

    //data provider for Volume tab
    @DataProvider(name = "volumeBaseUnits")
    public static Object[][] volumeBaseUnits() {

        List<String> volumeBaseUnits = new ArrayList<>();

        String[] elements = new String[] {
                "Cubic Meter",
                "Cubic Kilometer",
                "Cubic Centimeter",
                "Cubic Millimeter",
                "Liter",
                "Milliliter",
                "US Gallon",
                "US Quart",
                "US Pint",
                "US Cup",
                "US Fluid Ounce",
                "US Table Spoon",
                "Imperial Gallon",
                "Imperial Quart",
                "Imperial Pint",
                "Imperial Fluid Ounce",
                "Imperial Table Spoon",
                "Cubic Mile",
                "Cubic Yard",
                "Cubic Foot",
                "Cubic Inch"
        };

        volumeBaseUnits.addAll(Arrays.asList(elements));
        return new Object[][] {
                {volumeBaseUnits}
        };

    }
}
