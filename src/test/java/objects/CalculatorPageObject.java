package objects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.stream.Collectors;

public class CalculatorPageObject {

    public WebDriver driver;

    public CalculatorPageObject(WebDriver driver) {
        this.driver=driver;
    }

    //finding desired tab on its name
    public WebElement tab(String tabName) { return driver.findElement(By.xpath("//div[@id='topmenu']//a[text()='"+tabName+"']")); }

    //finding 'From' input field
    public WebElement fromInputField() { return driver.findElement(By.name("fromVal")); }

    //finding 'To' input fields
    public WebElement toInputField() { return driver.findElement(By.name("toVal")); }

    //finding container of initial units
    public WebElement initialUnitContainer() { return driver.findElement(By.name("calFrom")); }

    //finding container of desired units
    public WebElement desiredUnitContainer() { return driver.findElement(By.name("calTo")); }

    //gets names of all elements from the base unit container and puts them into a list
    public List<String> getVolumeBaseUnits() {
        final List<WebElement> volumeTabBaseUnits =
                driver.findElements(By.xpath("//select[@name='calFrom']/option"));

        return volumeTabBaseUnits
                .stream()
                .map(WebElement::getText)
                .collect(Collectors.toList());

    }
}
